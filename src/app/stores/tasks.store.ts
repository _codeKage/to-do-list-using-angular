import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Task} from '../../Models/Task';

import { findIndex } from 'lodash';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class TasksStore {

  private tasks$: BehaviorSubject<Task[]> = new BehaviorSubject([]);

  constructor(private http: HttpClient) {}

  init() {
    this.http.get('http://localhost/todo/getTasks.php').pipe(
      map((tasks: Task[]) => {
        return tasks.map((task: any) => {
          const taskObject = new Task();
          taskObject.task = task.task;
          taskObject.task_id = task.task_id;
          taskObject.dateCreated = task.created_at;
          taskObject.finishedAt = task.finished_at;
          return taskObject;
        });
      })
    ).subscribe((tasks: Task[]) => {
      this.storeAll(tasks);
    });
  }

  storeAll(tasks: Task[]) {
    this.tasks$.next(tasks);
  }

  getAll(): Observable<Task[]> {
    return this.tasks$;
  }

  update(task: Task, taskForm: FormData) {
    this.http.post('http://localhost/todo/editTask.php', taskForm).subscribe(() => {
     this.init();
    });
  }

  deleteTask(task: Task) {
    const taskForm = new FormData();
    taskForm.append('task_id', task.task_id.toString());
    this.http.post('http://localhost/todo/deleteTask.php', taskForm).subscribe(() => {
      this.init();
    });
  }

  addTask(task: Task) {
    const formData = new FormData();
    formData.append('task', task.task);
    this.http.post('http://localhost/todo/addTask.php', formData).subscribe(() => {
      this.init();
    });
  }

  changeIsFinished(task: Task) {
    const formData = new FormData();
    formData.append('task_id', task.task_id.toString());
    if (task.finishedAt === null) {
      this.http.post('http://localhost/todo/clearTask.php', formData).subscribe(result => {
        console.log(result);
      });
    } else {
      this.http.post('http://localhost/todo/undoTask.php', formData).subscribe(data => {
        console.log(data);
      });
    }
    this.init();
  }



}
