import {Component, OnInit} from '@angular/core';
import {Task} from '../Models/Task';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {EditTaskComponent} from './edit-task/edit-task.component';
import {HttpClient} from '@angular/common/http';
import {TasksStore} from './stores/tasks.store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  bsModalRef: BsModalRef;
  taskName: string;
  taskObject: Task;
  tasks: Task[] = [];
  title = 'To-Do-List-Angular6';

  constructor(private modalService: BsModalService,
              private http: HttpClient,
              private tasksStore: TasksStore) {}

  ngOnInit() {
    this.getTasks();
  }

  editTask(task: Task) {
    this.bsModalRef = this.modalService.show(EditTaskComponent);
    this.bsModalRef.content.task = task;
  }

  addTask(): void {
    this.taskObject = new Task();
    this.taskObject.task = this.taskName;
    this.tasksStore.addTask(this.taskObject);
  }


  deleteTask(task: Task): void {
      this.tasksStore.deleteTask(task);
  }
  changeIsFinished(task: Task): void {
    this.tasksStore.changeIsFinished(task);
  }
  getTasks(): void {
    this.tasksStore.init();
    this.tasksStore.getAll().subscribe(tasks => this.tasks = tasks);
  }
}
