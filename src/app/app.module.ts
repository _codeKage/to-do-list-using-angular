import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TasklistComponent } from './tasklist/tasklist.component';
import {FormsModule} from '@angular/forms';
import { EditTaskComponent } from './edit-task/edit-task.component';
import {ModalModule} from 'ngx-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {TasksStore} from './stores/tasks.store';

@NgModule({
  declarations: [
    AppComponent,
    TasklistComponent,
    EditTaskComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ModalModule.forRoot()
  ],
  entryComponents: [
    EditTaskComponent
  ],
  providers: [
    TasksStore
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
