import {Component, OnInit, Output} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import {Task} from '../../Models/Task';
import {TasksStore} from '../stores/tasks.store';

@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.css']
})
export class EditTaskComponent implements OnInit {
  taskName = ' ';
  task = new Task();

  constructor(public bsModalRef: BsModalRef,
              private tasksStore: TasksStore) { }

  ngOnInit() {
  }

  updateTask(task: Task) {
    const formData = new FormData();
    formData.append('task_id', task.task_id.toString());
    formData.append('task_name', this.taskName);
    task.task = this.taskName;
    this.tasksStore.update(task, formData);
    this.bsModalRef.hide();
  }

}
