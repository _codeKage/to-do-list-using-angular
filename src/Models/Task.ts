export class Task {
  task: string;
  finishedAt: string;
  dateCreated: string;
  task_id: number;
}
